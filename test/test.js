const test = require('ava');

const Script = require('..');

test.beforeEach((t) => {
  const script = new Script({});
  Object.assign(t.context, { script });
});

test('returns itself', (t) => {
  t.true(t.context.script instanceof Script);
});
