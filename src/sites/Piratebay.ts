import TorrentSite from '../TorrentSite'
import { encode } from 'urlencode'
import { getJsonFromUrl, parseTorrent, createMagnetUri, parseQueryString } from '../Utils'
import { SearchResult } from '../Interfaces'

class Piratebay extends TorrentSite {
  constructor() {
    super('https://apibay.org', '/q.php?q=%s', '/t.php?id=%s')
    this.name = Piratebay.name
  }

  async find(query: string, limit: number = 5): Promise<SearchResult[]> {
    if (query === undefined) return []
    query = parseQueryString(query)

    const searchResults = await getJsonFromUrl(this.getSearchPathQueryUrl(query))
    for (const result of searchResults) {
      if (this.torrentFilesQueue.length > limit) break
      const { id, name, info_hash, leechers, seeders } = result

      this.addTorrentToQueue(this.getTorrentFile(id, name, seeders, leechers, { info_hash }))
    }

    return await this.runScan()
  }

  async getTorrentFile(id: string, filename: string, seeders: number, leechers: number, optional?: any): Promise<SearchResult> {
    const { info_hash } = optional

    const url = this.getTorrentPathQueryUrl(id)
    const magnet = createMagnetUri(info_hash)
    const torrent = parseTorrent(magnet)

    return { filename, seeders, leechers, magnet, url, torrent}
  }
}

export default Piratebay
