import TorrentSite from '../TorrentSite'
import { parseTorrent, getDomDocumentFromUrl, parseQueryString } from '../Utils'
import { SearchResult } from '../Interfaces'

class Limetorrents extends TorrentSite {
  constructor() {
    super('https://www.limetorrents.info', '/search/all/%s/seeds/1/', '%s')
  }

  async find(query: string, limit: number = 5): Promise<SearchResult[]> {
    if (query === undefined) return []
    query = parseQueryString(query)

    const doc = await getDomDocumentFromUrl(this.getSearchPathQueryUrl(query))

    const items = doc.body.querySelectorAll(`.table2 tr[bgcolor]`)
    for (const item of items) {
      if (this.torrentFilesQueue.length > limit) break

      const seeders = parseInt(item.querySelector('.tdseed').textContent.replace(',', ''))
      const leechers = parseInt(item.querySelector('.tdleech').textContent.replace(',', ''))
      const { textContent: filename, href: torrentUri } = item.querySelector('div a:last-child')

      this.addTorrentToQueue(this.getTorrentFile(torrentUri, filename, seeders, leechers))
    }

    return await this.runScan()
  }

  async getTorrentFile(id: string, filename: string, seeders: number, leechers: number, optional?: any): Promise<SearchResult> {
    const doc = await getDomDocumentFromUrl(this.getTorrentPathQueryUrl(id))

    const url = this.getTorrentPathQueryUrl(id)
    const magnet = doc.body.querySelector(`a[href^='magnet:?']`).href
    const torrent = parseTorrent(magnet)

    return { filename, seeders, leechers, magnet, url, torrent}
  }
}

export default Limetorrents
