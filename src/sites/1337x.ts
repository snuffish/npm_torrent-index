import TorrentSite from "../TorrentSite"
import { SearchResult } from "../Interfaces"
import { parseQueryString } from "../Utils"
Object.fromEntries
class _1337x extends TorrentSite {
  constructor() {
    super('https://www.1337x.to', '/sort-search/%s/seeders/desc/1/', '/torrent/%s/%s')
  }

  async find(query: string, limit: number = 3): Promise<SearchResult[]> {
    if (query === undefined) return []
    query = await parseQueryString(query)
      console.log("DDD",query)

  }

  async getTorrentFile(id: string, filename: string, seeders: number, leechers: number, optional?: any): Promise<SearchResult> {

  }
}

export default _1337x
