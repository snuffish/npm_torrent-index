import { JSDOM } from 'jsdom'
import { ParsedDom } from './Interfaces'
import ParseTorrent from 'parse-torrent'
import fetch from 'node-fetch'
import { encode } from 'urlencode'

export const parseDom = (html: string): ParsedDom => {
  const dom = new JSDOM(html).window
  const { document: { body, head } } = dom

  return { dom, body, head }
}

export const getDomDocumentFromUrl = async (url: string): Promise<ParsedDom> => {
  const res = await fetch(url)
  const html = await res.text()

  return await parseDom(html)
}

export const getJsonFromUrl = async (url: string) => {
  const res = await fetch(url)
  return await res.json()
}

export const parseQueryString = (text: string) => encode(text.replace(/ /g, '-'))
export const createMagnetUri = (id: string) => `magnet:?xt=urn:btih:${ id }`
export const parseTorrent = (magnet: string) => ParseTorrent(magnet)
