import printf from 'printf'
import { SearchResult } from './Interfaces'
import { URL } from 'url'
import { parseQueryString } from './Utils'

abstract class TorrentSite {
  private _name: string
  private _base_url: string
  private searchPathQueryUrl: string
  private torrentPathQueryUrl: string
  private _torrentFilesQueue: Promise<SearchResult>[] = []

  constructor(base_url: string, searchPathQueryUrl: string, torrentPathQueryUrl: string) {
    this.name = new URL(base_url).hostname.split('.')[1]
    this._base_url = base_url
    this.searchPathQueryUrl = searchPathQueryUrl
    this.torrentPathQueryUrl = torrentPathQueryUrl
  }

  abstract async find(query: string, limit: number): Promise<SearchResult[]>
  abstract async getTorrentFile(id: string, filename: string, seeders: number, leechers: number, optional?: any): Promise<SearchResult>

  protected addTorrentToQueue = (torrent: Promise<SearchResult>) => this._torrentFilesQueue.push(torrent)

  protected getSearchPathQueryUrl = (query: string): string => this.base_url + printf(this.searchPathQueryUrl, parseQueryString(query))
  protected getTorrentPathQueryUrl = (id: string): string => this.base_url + printf(this.torrentPathQueryUrl, id)

  protected runScan = async (): Promise<SearchResult[]> => await Promise.all(this._torrentFilesQueue)

  public get name(): string {
    return this._name
  }

  public set name(value: string) {
    this._name = value
  }

  public get torrentFilesQueue(): Promise<SearchResult>[] {
    return this._torrentFilesQueue
  }

  private get base_url(): string {
    return this._base_url
  }
}

export default TorrentSite
