import { DOMWindow } from 'jsdom'

export interface ParsedDom {
    dom: DOMWindow,
    body: any,
    head: any
}

export interface SearchResult {
    filename?: string,
    seeders?: number,
    leechers?: number,
    magnet?: string,
    url?: string,
    torrent?: any
}
